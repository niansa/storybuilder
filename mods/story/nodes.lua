-- Normal drawtype if buildmode is enabled otherwise airlike
if buildmode == true then
	nodedrawtype = "glasslike"
else
	nodedrawtype = "airlike"
end

-- Checkpoint
minetest.register_node("story:checkpoint", {
	description = "Checkpoint",
	tiles ={"story_arrowpoint.png"},
	groups = {crumbly=3, soil=1},

    drawtype = nodedrawtype,
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    air_equivalent = true,
    pointable = buildmode,
    diggable = buildmode
})

-- Ice
minetest.register_node("story:ice", {
	description = "Ice",
	tiles ={"story_ice.png"},
	groups = {cracky=3},
})

-- Snow
minetest.register_node("story:snow", {
	description = "Snow",
	tiles ={"story_snow.png"},
	groups = {crumbly=3, falling_node=1},
})

-- Snow
minetest.register_node("story:lines", {
	description = "Lines",
	tiles ={"story_lines.png"},
	groups = {snappy=2, cracky=3, oddly_breakable_by_hand=3},
	drawtype = "glasslike",
	paramtype = "light",
	sunlight_propagates = true,
	light_source = 13,
})
