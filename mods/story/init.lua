basicprivs = {interact = true}
story = {}
story.stagepos = {}
local modpath = minetest.get_modpath("story")
dofile(modpath.."/schem.lua")
dofile(modpath.."/checkpoint_api.lua")
dofile(modpath.."/stage_api.lua")
dofile(modpath.."/nodes.lua")
dofile(modpath.."/commands.lua")
dofile(modpath.."/special_nodes.lua")
dofile(modpath.."/borders.lua")

