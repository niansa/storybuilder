local modpath = minetest.get_modpath("story")
local worldpath = minetest.get_worldpath()

-- round
function story.get_random_pos()
	return math.random(-30000,30000)
end
-- get_stage
function story.get_stage(stage)
	randpos1 = story.get_random_pos()
	randpos2 = story.get_random_pos()
	if loadfile(worldpath.."/stages/"..stage..".pos") then
		dofile(worldpath.."/stages/"..stage..".pos")
		return true
	else
		return false
	end
end
function story.clean_stagevars()
--	spos = nil  (TODO: find out why this is causing weird bugs)
    we_file = nil
	music = nil
	minstage = nil
end
function story.get_player_relative_pos(name)
	local object = minetest.get_player_by_name(name)
	positions = object:get_pos()
	local rel_pos = { x = positions["x"] - story.stagepos[name]["x"], y = positions["y"] - story.stagepos[name]["y"], z = positions["z"] - story.stagepos[name]["z"]}
	positions = {}
	return rel_pos
end
-- reload_stage
function story.reload_stage(name, continuation)
	if noreload == true then
		minetest.chat_send_player(name, "Not reloading stage as reload is disabled")
		return false
	end
	spos = {}
	local object = minetest.get_player_by_name(name)
--	Load informations about the stage
	local stage = storybase.get_stage(name)
--	Get stage positions
	storybase.debugmsg(name, "Loading stage " .. stage .. "...")
	if not story.get_stage(stage) then
		storybase.crashmsg(name, "reload_stage", "No such stage", stage)
		return false
	end
--	Stop playing music (if old music ~= new music)
	if music ~= bgmusic.playing("title", name) then
		bgmusic.stop(name)
	end
--	Show loading texhud and teleport player to random position
	local loadinghud = playertools.texhud_add(object, "stage_loading.png", true)
	object:set_pos({ x = story.get_random_pos(), y = story.get_random_pos(), z = story.get_random_pos()})
--	Place matching schematic
	storybase.debugmsg(name, "Loading schematic, please be patient...")
	if we_file then
		schem.load("", spos, we_file)
	else
		schem.load("", spos, "stage_"..stage)
	end
--	Check if player has checkpoint, if yes, use it
	local checkpoint = story.get_checkpoint(name)
	if checkpoint and continuation then
		spos.x_off = checkpoint["x"]
		spos.y_off = checkpoint["y"]
		spos.z_off = checkpoint["z"]
	end
--	Teleport the player to the schematic
	storybase.debugmsg(name, "Done! Positions: "..spos.x.." "..spos.y.." "..spos.z)
	object:set_pos({ x = spos.x + spos.x_off, y = spos.y + spos.y_off, z = spos.z + spos.z_off })
--	Internally store stage positions
	story.stagepos[name] = { x = spos.x, y = spos.y, z = spos.z }
--	Remove loading screen
	playertools.texhud_remove(object, loadinghud, true)
--	Start playing music (if old music ~= new music)
	if music ~= bgmusic.playing("title", name) then
		bgmusic.play(name, music)
	end
--	Remove checkpoint if not continuation
	if not continuation then
		story.set_checkpoint(name, false)
	end
	story.clean_stagevars()
	return true
end
