-- Blinking lines

minetest.register_abm({
	nodenames = {"story:lines_light", "story:lines_dark"},
	interval = 0.5,
	chance = 1,
	action = function(pos, node, active_object_count,
		active_object_count_wider)
		if node.name == "story:lines_light" then
			minetest.set_node(pos, {name = "story:lines_dark"})
		else
			minetest.set_node(pos, {name = "story:lines_light"})
		end
	end
})

minetest.register_node("story:lines_light", {
	tiles ={"story_lines.png"},
	drops = "story:lines_dark",
	drawtype = "glasslike",
	paramtype = "light",
	light_source = 13,
	sunlight_propagates = true,
	groups = {snappy=2, cracky=3, oddly_breakable_by_hand=3},
})
minetest.register_node("story:lines_dark", {
	description = "Lines (blinking)",
	tiles ={"story_lines.png"},
	drawtype = "glasslike",
	paramtype = "light",
        alpha = 50,
	sunlight_propagates = true,
	groups = {snappy=2, cracky=3, oddly_breakable_by_hand=3},
})

-- Cold water
minetest.register_node("story:coldwater_flowing", {
	description = "Cold water (flowing)",
	inventory_image = minetest.inventorycube("default_water.png"),
	drawtype = "flowingliquid",
	tiles ={"default_water.png"},
	special_tiles = {
		{
			image="default_water.png",
			backface_culling=false,
		},
		{
			image="default_water.png",
			backface_culling=true,
		},
	},
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "story:coldwater_flowing",
	liquid_alternative_source = "story:coldwater_source",
	liquid_viscosity = LAVA_VISC,
	damage_per_second = 4*2,
	post_effect_color = {a=192, r=0, g=64, b=255},
	groups = {liquid=2, hot=3},
})
minetest.register_node("story:coldwater_source", {
	description = "Cold water",
	inventory_image = minetest.inventorycube("default_water.png"),
	drawtype = "liquid",
	tiles = {
		{
			name = "default_water.png"
		}
	},
	special_tiles = {
		{name="default_water.png", backface_culling=false},
	},
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "story:coldwater_flowing",
	liquid_alternative_source = "story:coldwater_source",
	liquid_viscosity = LAVA_VISC,
	damage_per_second = 4*2,
	post_effect_color = {a=192, r=0, g=64, b=255},
	groups = {liquid=2, hot=3},
})
