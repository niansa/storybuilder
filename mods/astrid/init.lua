-- Regenbogen
minetest.register_node("astrid:rainbowcat", {
	description = "Rainbowcat",
	tiles ={"default_nc_side.png", "default_nc_side.png", "default_nc_side.png",
		"default_nc_side.png", "default_nc_back.png", "default_nc_front.png"},
	inventory_image = "default_nc_front.png",
	paramtype2 = "facedir",
	groups = {cracky=2},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_defaults(),
})

minetest.register_node("astrid:rainbow", {
	description = "Rainbow",
	tiles ={"default_nc_rb.png"},
	inventory_image = "default_nc_rb.png",
	is_ground_content = false,
	groups = {cracky=2},
	sounds = default.node_sound_defaults(),
})

minetest.register_node("astrid:bluekitty", {
	description = "Bluekitty",
	tiles = {"astrid_bluekitty.png"},
	is_ground_content = false,
	groups = {cracky=2},
	sounds = default.node_sound_defaults(),
})
