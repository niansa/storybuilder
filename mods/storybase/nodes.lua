-- Normal drawtype if buildmode is enabled otherwise airlike
if buildmode == true then
	nodedrawtype = "glasslike"
else
	nodedrawtype = "airlike"
end

-- Create function to quickly register a stageblock
function register_stageblock(stagename)
	minetest.register_node("storybase:stageblock_"..stagename, {
		description = "Stageblock ("..stagename..")",
		tiles ={"story_arrowup.png"},
		groups = {crumbly=3, soil=1},
		
		drawtype = nodedrawtype,
		paramtype = "light",
		sunlight_propagates = true,
		walkable = false,
		air_equivalent = true,
		pointable = buildmode,
		diggable = buildmode
	})
end

-- Create function to quickly register a scoreblock
function register_scoreblock(score)
	minetest.register_node("storybase:scoreblock_" .. score, {
		description = "Scoreblock (" .. score .. ")",
		tiles ={"test.png"},
		groups = {crumbly=3, soil=1},

        on_dig = function(pos, node, player)
			if buildmode == true then
				minetest.node_dig(pos, node, player)
				return
			end
			storybase.inc_score(player:get_player_name(), score)
			minetest.node_dig(pos, node, digger)
		end
	})

end

-- Register all stageblocks
for index, stagename in pairs(storybase.list_stages()) do
	register_stageblock(stagename)
end

-- Register 100 scoreblocks
local i = 1
while i ~= 100 do
	register_scoreblock(i)
	i = i + 1
end
