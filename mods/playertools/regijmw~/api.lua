corruption = {}

-- handle
function corruption.handle(name)
--	Flag player as corrupted
	print("[corruptionhandler] Wow. Something terrible happend and the playerdata of "..name.." got corrupted. :sob:")
	print("[corruptionhandler] Trying to recover...")
	minetest.chat_send_player(name, "Your playerdata is corrupted and being recovered now.")
	corruption.flag(name)
--	Try to fix
	if corruption.fix(name) then
		print("[corruptionhandler] Sucess")
		minetest.chat_send_player(name, "Your playerdata was recovered succesfully.")
		return true
	else
		print("[corruptionhandler] Failed, asking the player what to do...")
		minetest.chat_send_player(name, "Your playerdata could not be recovered.\nHere are a few commands for you to help fixing that issue:")
		corruption.send_info(name)
		return false
	end
end

-- send_info
function corruption.send_info(name)
--	Get current privileges
	local privs = minetest.get_player_privs(name)
--	Show info
	if privs.cheat == true then
		minetest.chat_send_player(name, "/getstage – See which stage you are currently in")
		minetest.chat_send_player(name, "/loadstage <stage> – Load a stage ignoring all restrictions")
	end
--	minetest.chat_send_player(name, "/corruptionfix – Try to fix playerdata corruption")
	minetest.chat_send_player(name, "/gamereset – Delete all your data and restart the game from the beginning")
	if allow_cheating == true then
		minetest.chat_send_player(name, "Allow cheats (/cheating allow) and run /corruptionhelp to see advanced options")
	end
	return true
end

-- fix
function corruption.fix(name)
	local backup_stage = storybase.get_backup_stage(name)
	local worldpath = minetest.get_worldpath()
	if loadfile(worldpath.."/stages/"..backup_stage..".pos") then
		storybase.set_stage(name, backup_stage)
		story.reload_stage(name)
		return true
	end
	return false
end

-- remove_player
function corruption.remove_player(name)
	minetest.after(1, function()
--		Remove player
		local removal = minetest.remove_player(name)
		print("[corruptionhandler] The player '"..name.."' has been removed: "..removal)
	end)
end


-- flag
function corruption.flag(name)
--	Get current privileges
	local privs = minetest.get_player_privs(name)
--	Grant "corruption" privilege
	privs.corruption = true
	if not minetest.check_player_privs(name,{corruption=true}) then
		minetest.set_player_privs(name, privs)
		print("[corruptionhandler] "..name.." has been flagged as corrupted.")
	end
end

-- unflag
function corruption.unflag(name)
--	Get current privileges
	local privs = minetest.get_player_privs(name)
--	Revoke "corruption" privilege
	privs.corruption = nil
	if minetest.check_player_privs(name,{corruption=true}) then
		minetest.set_player_privs(name, privs)
		print("[corruptionhandler] "..name.." has been unflagged as corrupted.")
	end
end
