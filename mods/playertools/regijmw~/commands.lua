-- gamereset
minetest.register_chatcommand("gamereset", {
	privs = "corruption",
	func = function(name, param)
		if not minetest.is_singleplayer() then
			minetest.kick_player(name, "Your gamedata is being removed, so you have to reconnect now.")
			corruption.remove_player(name)
			return true
		else
			return false, "You are in singleplayer mode, so your game data can't be removed."
		end
	end
})
