playertools = {}

-- remove_player
function playertools.remove(name)
	minetest.after(1, function()
		storybase.debugmsg(name, "player is being removed...")
		local removal = minetest.remove_player(name)
	end)
end
-- texhud_add
function playertools.texhud_add(object, hudname, hudhide)
	local texhud = object:hud_add({
		hud_elem_type = "image",
		position      = {x = 0.5, y = 0.5},
		offset        = {x = 0,   y = 0},
		text          = hudname,
		alignment     = {x = 0, y = 0},  -- center aligned
		scale         = {x = 1.5, y = 1.5}, -- covered late
		number = 0xD61818,
	})
	if hudhide == true then
		object:hud_set_flags(defaultnohud)
	end
	return texhud
end
-- texhud_remove
function playertools.texhud_remove(object, hud, hudunhide)
	minetest.after(0.5,function()
	object:hud_remove(hud)
	if hudunhide == true then
		object:hud_set_flags(defaulthud)
	end
	end, object, hud, hudunhide)
	return true
end
